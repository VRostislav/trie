/** Copyright &copy; 2017, rostislav.vel@gmail.com.
 * \brief  Тестовое приложение проверки реализации дерева.
 * \author Величко Ростислав
 * \date   14.12.2017
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include <memory>
#include <iostream>
#include <sstream>
#include <random>
#include <ctime>

#include "Log.hpp"
#include "Trie.hpp"


#define DEFAULT_NUM_GENERATED_STRINGS 50
#define MIN_NUM_GENERATED_STRINGS 10
#define MAX_NUM_GENERATED_STRINGS 10000
#define MIN_LEN_STRINGS 1


struct GlobalArgs {
    int _nemGeneratedStrings; /// параметр -n
} __global_args;

static const char *__opt_string = "n:h?";


void HelpMessage() {
    std::cout << "  Use:\n\t#trie -h 50\n"
              << "  Args:\n"
              << "\t[-n]\t Generate strings num. [" << MIN_NUM_GENERATED_STRINGS << " <-> " << MAX_NUM_GENERATED_STRINGS << "].\n";
    exit(EXIT_FAILURE);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


struct CStringArray {
    char **_c_strs;
    int *_values;
    int _length;
};


CStringArray Generate(int num_gen_strs_) {
    LOG(DEBUG) << num_gen_strs_;
    CStringArray cstrarr;
    cstrarr._length = num_gen_strs_;
    cstrarr._c_strs = new char*[cstrarr._length];
    cstrarr._values = new int[cstrarr._length];
    for (int i = 0; i < num_gen_strs_; ++i) {
        cstrarr._values[i] = std::rand();
        int str_len = 2 + (rand() % ALPHABETS);
        cstrarr._c_strs[i] = new char[str_len];
        cstrarr._c_strs[i][str_len - 1] = 0;
        for (int s = 0; s < str_len - 1; ++s) {
            cstrarr._c_strs[i][s] = static_cast<char>(CASE + (std::rand() % ALPHABETS));
        }
        LOG(DEBUG) << "[" << i << "] " << (str_len - 1) << "; key: " << cstrarr._c_strs[i] << " - v: " << cstrarr._values[i];
    }
    return cstrarr;
}


void Clear(CStringArray &cstrarr_) {
    LOG(DEBUG);
    for (int i = 0; i < cstrarr_._length; ++i) {
        delete[] cstrarr_._c_strs[i];
    }
    delete[] cstrarr_._values;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


int main(int argc_, char **argv_) {
    LOG_TO_STDOUT;
    int opt = 0;

    /// Инициализация globalArgs до начала работы с ней.
    __global_args._nemGeneratedStrings = DEFAULT_NUM_GENERATED_STRINGS;      

    /// Обработка входных опций.
    opt = getopt(argc_, argv_, __opt_string);
    while(opt != -1) {
        int value = 0;
        switch(opt) {
            case 'n':
                value = strtol(optarg, (char**)NULL, 10);
                if (not value or (MIN_NUM_GENERATED_STRINGS > value) or (MAX_NUM_GENERATED_STRINGS < value)) { 
                    HelpMessage();
                }
                __global_args._nemGeneratedStrings = value;
                break;

            case 'h':
            case '?':
                HelpMessage();
                break;
            default: break;
        }
        opt = getopt(argc_, argv_, __opt_string);
    }

    Trie trie(ALPHABETS);
    
    /// Сгенерить строки.
    std::srand(static_cast<unsigned>(std::time(nullptr)));
    CStringArray cstrarr = Generate(__global_args._nemGeneratedStrings);
    
    /// Загрузить строк в tree.
    for (int i = 0; i < cstrarr._length; ++i) {
        trie.insert(cstrarr._c_strs[i], cstrarr._values[i]);
    }

    /// Удалить произвольный элемент.
    int id_erase_str = rand() % cstrarr._length;
    trie.erase(cstrarr._c_strs[id_erase_str]);

    /// Поискать удалённый элемент.
    trie.find(cstrarr._c_strs[id_erase_str]);

    /// Найти произвольный элемент, отличный от удалённого.
    int id_str;
    while ((id_str = rand() % cstrarr._length) == id_erase_str) {}
    trie.find(cstrarr._c_strs[id_str]);
    
    /// Удалить тестовые данные.
    Clear(cstrarr);
    return EXIT_SUCCESS;
}

/// Пример вывода логов.
/// rostislav@brain:~/develop/trie/bld/Release/bin$ ./trie -n 20
/// 1. [18.12.2017-17:06:22.829839] [DEBUG] [CStringArray Generate(int)] 20
/// 2. [18.12.2017-17:06:22.829864] [DEBUG] [CStringArray Generate(int)] [0] 1; key: n - v: 487836317
/// 3. [18.12.2017-17:06:22.829872] [DEBUG] [CStringArray Generate(int)] [1] 13; key: tjzouhhnamjbc - v: 2136171351
/// 4. [18.12.2017-17:06:22.829877] [DEBUG] [CStringArray Generate(int)] [2] 11; key: pqjsiwohnml - v: 1435689834
/// 5. [18.12.2017-17:06:22.829884] [DEBUG] [CStringArray Generate(int)] [3] 13; key: zlasxzgrgpeib - v: 827782820
/// 6. [18.12.2017-17:06:22.829974] [DEBUG] [CStringArray Generate(int)] [4] 10; key: elwvbfnkde - v: 1082021917
/// 7. [18.12.2017-17:06:22.829998] [DEBUG] [CStringArray Generate(int)] [5] 17; key: qekcfxcyubgmjysrz - v: 755725457
/// 8. [18.12.2017-17:06:22.830027] [DEBUG] [CStringArray Generate(int)] [6] 4; key: fvza - v: 1998890798
/// 9. [18.12.2017-17:06:22.830035] [DEBUG] [CStringArray Generate(int)] [7] 7; key: qjjuczk - v: 1708782813
/// 10. [18.12.2017-17:06:22.830041] [DEBUG] [CStringArray Generate(int)] [8] 12; key: ooiqodtwreuj - v: 1372402468
/// 11. [18.12.2017-17:06:22.830047] [DEBUG] [CStringArray Generate(int)] [9] 23; key: tabpbeohuzqqdralcobngrs - v: 169391557
/// 12. [18.12.2017-17:06:22.830052] [DEBUG] [CStringArray Generate(int)] [10] 17; key: jfmuciqdmhesvomwe - v: 477194927
/// 13. [18.12.2017-17:06:22.830058] [DEBUG] [CStringArray Generate(int)] [11] 26; key: wemauqnchfdzqimlmudrikyahm - v: 1969834778
/// 14. [18.12.2017-17:06:22.830063] [DEBUG] [CStringArray Generate(int)] [12] 4; key: qsco - v: 1151766994
/// 15. [18.12.2017-17:06:22.830069] [DEBUG] [CStringArray Generate(int)] [13] 15; key: psgeuomznejbpwy - v: 225215482
/// 16. [18.12.2017-17:06:22.830074] [DEBUG] [CStringArray Generate(int)] [14] 14; key: geljlxxoqsseqj - v: 284761094
/// 17. [18.12.2017-17:06:22.830090] [DEBUG] [CStringArray Generate(int)] [15] 12; key: pcffohvsqwkm - v: 1666184125
/// 18. [18.12.2017-17:06:22.830096] [DEBUG] [CStringArray Generate(int)] [16] 7; key: cdlnmyn - v: 1347196572
/// 19. [18.12.2017-17:06:22.830102] [DEBUG] [CStringArray Generate(int)] [17] 16; key: ddhjwshhkjopzvkt - v: 835748795
/// 20. [18.12.2017-17:06:22.830108] [DEBUG] [CStringArray Generate(int)] [18] 10; key: fafmcizsux - v: 1406571308
/// 21. [18.12.2017-17:06:22.830113] [DEBUG] [CStringArray Generate(int)] [19] 9; key: mknwvjocq - v: 1401087745
/// 22. [18.12.2017-17:06:22.830119] [DEBUG] [void Trie::insert(const char*, int)] {"n":487836317}
/// 23. [18.12.2017-17:06:22.830126] [DEBUG] [void Trie::insert(const char*, int)] {"tjzouhhnamjbc":2136171351}
/// 24. [18.12.2017-17:06:22.830143] [DEBUG] [void Trie::insert(const char*, int)] {"pqjsiwohnml":1435689834}
/// 25. [18.12.2017-17:06:22.830154] [DEBUG] [void Trie::insert(const char*, int)] {"zlasxzgrgpeib":827782820}
/// 26. [18.12.2017-17:06:22.830179] [DEBUG] [void Trie::insert(const char*, int)] {"elwvbfnkde":1082021917}
/// 27. [18.12.2017-17:06:22.830189] [DEBUG] [void Trie::insert(const char*, int)] {"qekcfxcyubgmjysrz":755725457}
/// 28. [18.12.2017-17:06:22.830200] [DEBUG] [void Trie::insert(const char*, int)] {"fvza":1998890798}
/// 29. [18.12.2017-17:06:22.830206] [DEBUG] [void Trie::insert(const char*, int)] {"qjjuczk":1708782813}
/// 30. [18.12.2017-17:06:22.830215] [DEBUG] [void Trie::insert(const char*, int)] {"ooiqodtwreuj":1372402468}
/// 31. [18.12.2017-17:06:22.830224] [DEBUG] [void Trie::insert(const char*, int)] {"tabpbeohuzqqdralcobngrs":169391557}
/// 32. [18.12.2017-17:06:22.830238] [DEBUG] [void Trie::insert(const char*, int)] {"jfmuciqdmhesvomwe":477194927}
/// 33. [18.12.2017-17:06:22.830249] [DEBUG] [void Trie::insert(const char*, int)] {"wemauqnchfdzqimlmudrikyahm":1969834778}
/// 34. [18.12.2017-17:06:22.830264] [DEBUG] [void Trie::insert(const char*, int)] {"qsco":1151766994}
/// 35. [18.12.2017-17:06:22.830269] [DEBUG] [void Trie::insert(const char*, int)] {"psgeuomznejbpwy":225215482}
/// 36. [18.12.2017-17:06:22.830281] [DEBUG] [void Trie::insert(const char*, int)] {"geljlxxoqsseqj":284761094}
/// 37. [18.12.2017-17:06:22.830291] [DEBUG] [void Trie::insert(const char*, int)] {"pcffohvsqwkm":1666184125}
/// 38. [18.12.2017-17:06:22.830301] [DEBUG] [void Trie::insert(const char*, int)] {"cdlnmyn":1347196572}
/// 39. [18.12.2017-17:06:22.830307] [DEBUG] [void Trie::insert(const char*, int)] {"ddhjwshhkjopzvkt":835748795}
/// 40. [18.12.2017-17:06:22.830321] [DEBUG] [void Trie::insert(const char*, int)] {"fafmcizsux":1406571308}
/// 41. [18.12.2017-17:06:22.830330] [DEBUG] [void Trie::insert(const char*, int)] {"mknwvjocq":1401087745}
/// 42. [18.12.2017-17:06:22.830343] [DEBUG] [void Trie::erase(const char*)] Erase node [0]: 'a'
/// 43. [18.12.2017-17:06:22.830349] [DEBUG] [void Trie::erase(const char*)] Erase node [25]: 'z'
/// 44. [18.12.2017-17:06:22.830353] [DEBUG] [void Trie::erase(const char*)] Erase node [21]: 'v'
/// 45. [18.12.2017-17:06:22.830359] [DEBUG] [int Trie::find(const char*)] {"fvza":-1}
/// 46. [18.12.2017-17:06:22.830365] [DEBUG] [int Trie::find(const char*)] {"ddhjwshhkjopzvkt":835748795}
/// 47. [18.12.2017-17:06:22.830370] [DEBUG] [void Clear(CStringArray&)] 
/// Log is stopped.
