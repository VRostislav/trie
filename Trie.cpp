#include <cstring>
#include <iostream>

#include "Log.hpp"
#include "Trie.hpp"

#include <iostream>
#include <string>
#include <map>
#include <vector>


#include <cstdio>
#include <cstdlib>
#include <vector>


Trie::IntVector::IntVector() 
    : _arr(nullptr)
    , _real_size(0)
    , _length(0) 
{}


Trie::IntVector::~IntVector() {
    if (_arr) {
        delete[] _arr;
    }
}


int Trie::IntVector::operator[](size_t id_) {
    if (_arr and _length and id_ < _length) {
        return _arr[id_];
    }
    return 0;
}


void Trie::IntVector::push_back(int value_) {
    if (_real_size < _length + 1) {
        _real_size = _length + static_cast<int>(_length * 0.2);
        int *arr = new int[_real_size];
        for (size_t i = 0; i < _length; ++i) {
            arr[i] = _arr[i];
        }
        delete[] _arr;
        _arr = arr;
    }
    _arr[_length] = value_;
    ++_length;
}


void Trie::IntVector::pop_back() {
    if (_length) {
        --_length;
    }
}


size_t Trie::IntVector::size() {
    return _length;
}


Trie::Node::Node(int alen_) 
    : _alen(alen_)
    , _parent(nullptr) {
    _childrens = new PNode[_alen];
    for (int i = 0; i < _alen; ++i) {
        _childrens[i] = nullptr;
    }
}


Trie::Node::~Node() {
    for(int i = 0; i < _alen; ++i) {
        if (_childrens[i]) {
            delete _childrens[i];
        }
    }
    delete[] _childrens;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


Trie::Node* Trie::search(const char *key_) {
    PNode node = _root;
    int i = 0;
    while (key_[i] not_eq '\0') {
        int id = key_[i] - CASE;
        if (id < ALPHABETS) {
            if (node->_childrens[id] not_eq nullptr) {
                node = node->_childrens[id];
            } else {
                break;
            }
        }
        ++i;
    }
    if (not (key_[i] == '\0' and node->_occurrences.size() not_eq 0)) {
        /// Ключь не найден.
        node = nullptr;
    }
    return node;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


Trie::Trie(int alphabets_) 
    : _alphabets(alphabets_)
{
    _root = new Node(_alphabets);
}


Trie::~Trie() {
    delete _root;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void Trie::insert(const char *key_, int value_) {
    LOG(DEBUG) << "{\"" << key_ << "\":" << value_ << "}";
    PNode node = _root;
    int i = 0;
    while (key_[i] not_eq '\0') {
        int id = key_[i] - CASE;
        if (id < ALPHABETS) {
            if (node->_childrens[id] == nullptr) {
                node->_childrens[id] = new Node(_alphabets);
                node->_childrens[id]->_parent = node;
            }
            node = node->_childrens[id];
        }
        ++i;
    }
    node->_occurrences.push_back(value_);
}


int Trie::find(const char *key_) {
    int value = -1;
    PNode node = search(key_);
    if (node) {
        value = node->_occurrences[node->_occurrences.size() - 1];
    }
    LOG(DEBUG) << "{\"" << key_ << "\":" << value << "}";
    return value;
}


void Trie::erase(const char *key_) {
    PNode node = search(key_);
    if (node) {
        node->_occurrences.pop_back();
        bool no_child = true;
        int child_count = 0;
        int i;
        for (i = 0; i < _alphabets; ++i) {
            if (node->_childrens[i] not_eq nullptr) {
                no_child = false;
                ++child_count;
            }
        }
        if (no_child) {
            PNode parent;
            while (node->_occurrences.size() == 0 and node->_parent not_eq nullptr and child_count == 0) {
                child_count = 0;
                parent = node->_parent;
                for (i = 0; i < _alphabets; ++i) {
                    if (parent->_childrens[i] not_eq nullptr) {
                        if (node == parent->_childrens[i]) {
                            parent->_childrens[i] = nullptr;
                            delete node;
                            node = parent;
                            LOG(DEBUG) << "Erase node [" << i << "]: '" << static_cast<char>(i + CASE) << "'";
                        } else {
                            ++child_count;
                        }
                    }
                }
            }
        } else {
            LOG(WARNING) << "Can`t erase \"" << key_ << "\"";
        }
    }
}


void Trie::clear() {
}
