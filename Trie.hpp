/** Copyright &copy; 2017, rostislav.vel@gmail.com.
 * \brief  Класс реализующий вункции дерева.
 * \author Величко Ростислав
 * \date   14.12.2017
 */

#include <memory>

#define ALPHABETS 26
#define CASE 97


class Trie { 
    int _alphabets;
    
    class IntVector {
        int *_arr;
        size_t _real_size;
        size_t _length;
        
    public:
        IntVector();
        virtual ~IntVector();

        int operator[](size_t id_);
        void push_back(int value_);
        void pop_back();
        size_t size();
    };
    
    struct Node;
    typedef Node* PNode;
    
    struct Node {
        int _alen;
        Node(int alen_);
        virtual ~Node();
        
        PNode _parent;
        PNode *_childrens;
        IntVector _occurrences;
    };
    
    PNode _root;
    
    /**
     * \brief  Метод ищет значения по ключу.
     * \return Найденная нода бора.
     */
    Node* search(const char *key_);


public:
    /**
     * \brief  Конструктор инициализирует для начала работы внутренние компоненты.
     * \param alphabets_ Размер алфавита.
     */
    explicit Trie(int alphabets_ = ALPHABETS);
    virtual ~Trie();
    
    /**
     * \brief  Метод добавляет новое или заменяет значение по ключу.
     * \param key_ Новый ключ.
     * \param value_ Значение ключа.
     */
    void insert(const char *key_, int value_);

    /**
     * \brief  Метод удаляет значение и ключь.
     * \param key_ Новый ключ.
     */
    void erase(const char *key_);

    /**
     * \brief  Метод ищет значения по ключу.
     * \return Найденное значение.
     */
    int find(const char *key_);
    
    /**
     * \brief  Метод очищает дерево.
     */
    void clear();
};
